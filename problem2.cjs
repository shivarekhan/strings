
// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and 
//return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function problem2(str) {
    if (str.length === 0) {
        return 0;
    }
    let auxString = "";
    let arrResult = [];
    for (let i = 0; i < str.length; i++) {
        if(str[i]=="."){
            arrResult.push(Number(auxString));
            auxString="";
        }
        else{
            auxString = auxString+str[i];
        }
    }
    arrResult.push(Number(auxString));

    return arrResult;
}
module.exports = problem2;