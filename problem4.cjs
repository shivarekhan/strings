// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
function problem4(str){
    let buffStr = "";
    if(str.length==0){
        return buffStr;
    }

    for(let i in str){
        buffStr = buffStr+" "+str[i];
    }
    return buffStr;
}
module.exports = problem4;