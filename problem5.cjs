// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.
function problem5(arr) {
    if (arr.length == 0) {
        return [];
    }
    let buffstr = arr[0];
    for (let i = 1; i < arr.length; i++) {
        buffstr =buffstr+" "+ arr[i];
    }
    return buffstr;
}
module.exports = problem5;