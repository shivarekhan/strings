// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.
function problem3(str){
    if (str.length === 0) {
        return 0;
    }
    let auxString = "";
    let arrResult = [];
    for (let i = 0; i < str.length; i++) {
        if(str[i]=="/"){
            arrResult.push(Number(auxString));
            auxString="";
        }
        else{
            auxString = auxString+str[i];
        }
    }
    arrResult.push(Number(auxString));

    return arrResult[1];
}
module.exports = problem3;