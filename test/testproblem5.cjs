const problem5 = require('../problem5.cjs');
let result1 = problem5([]);
if(result1.length==0){
    console.log("input array is empty");
}
else{
    console.log(result1);
}

let result2 = problem5(["the", "quick", "brown", "fox"]);
if(result2.length==0){
    console.log("input array is empty");
}
else{
    console.log(result2);
}