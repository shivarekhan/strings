const problem3 = require('../problem3.cjs');
let result1 = problem3("1/10/2000");
if (result1 == 0|| (result1<0 || result1>12)) {
    console.log("input string is invalid");
}
else {
    console.log(result1);
}

let result2 = problem3("23/45/2000");
if (result2 == 0 || (result2<0 || result2>12)) {
    console.log("input string is invalid");
}
else {
    console.log(result2);
}


let result3 = problem3("23/-1/2000");
if (result3 == 0 || (result3<0 || result3>12)) {
    console.log("input string is invalid");
}
else {
    console.log(result3);
}
