const problem4 = require('../problem4.cjs');
let result1 = problem4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});
if(result1.length==0){
    console.log("input object is empty");
}
else{
    console.log(result1);
}

let result2 = problem4({"first_name": "JoHN", "last_name": "SMith"});
if(result2.length==0){
    console.log("input object is empty");
}
else{
    console.log(result2);
}