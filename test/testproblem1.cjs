const problem1 = require('../problem1.cjs');

let result1 = problem1("$100.45");
if(result1==""){
    console.log("input string is empty or invalid");
}
else{
    console.log(result1);
}

let result2 = problem1("-$123");
if(result2==""){
    console.log("input string is empty or invalid");
}
else{
    console.log(result2);
}