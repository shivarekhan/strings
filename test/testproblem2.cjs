const problem2 = require('../problem2.cjs');
let result1 = problem2("111.139.161.143");
if (result1 == 0) {
    console.log("input string is invalid");
}
else {
    console.log(result1);
}

let result2 = problem2("");
if (result2 == 0) {
    console.log("input string is invalid");
}
else {
    console.log(result2);
}
