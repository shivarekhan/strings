// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", 
//and so on. Write a function to convert the given strings into their equivalent numeric 
//format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing 
//mistakes in the string so if the number is invalid, return 0. 
function problem1(str){
    if(str.length==0)return "";
    let buffer = "";
    for(let i=0;i<str.length;i++){
        if((str[i]<='9' && str[i]>='0') || str[i]=='.' || str[i]=='-'){
            buffer = buffer+str[i];
        }
    }
    return buffer;

}
module.exports = problem1;